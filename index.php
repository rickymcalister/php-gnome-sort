<?php

	/**
	 * Initialise a data array with a sequential set of integers, then shuffle to create an un-sorted 
	 * list for the purpose of this example..
	 */
	for ($i = 1; $i <= 20; $i++) {
		$data[] = $i;
	}

	shuffle($data);

	// Output the initially un-sorted list.
	echo "<pre>";
	print_r($data);
	echo "</pre>";

	// Determine the start time of the function.
	$start = microtime(true);

	// Sort the data.
	$sortedData = gnomeSort($data);

	// Calculate the time taken to execute the sort function
	$duration = microtime(true) - $start;

	echo "Duration : $duration s";

	// Output the sorted data.
	echo "<pre>";
	print_r($sortedData);
	echo "</pre>";

	/**
	 * Perform a gnome sort on an array of simple data types.
	 *
	 * @param array $data This is an array of simple data types, e.g. integers
	 * @return array
	 */
	function gnomeSort($data) {
		$position = 1;

		// Iterate through the data array until the last element is reached.
		while ($position < count($data)) {
			if ($data[$position] >= $data[$position - 1]) {
				/**
				 * The value of the element at the current position is greater than or 
				 * equal to it's predecessor and is therefore in the correct sequence 
				 * relative to the element before, so simply move onto the next position.
				 */
				$position++;
			} else {
				/** 
				 * Swap the values of the element at the current position and the element 
				 * directly before it.
				 */
				$tmpValue = $data[$position];
				$data[$position] = $data[$position - 1];
				$data[$position - 1] = $tmpValue;

				// The set needs to be re-examined and compared from the previous position.
				if ($position > 1) {
					$position--;
				}
			}
		}

		return $data;
	}

?>